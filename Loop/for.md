# For

* Boucle qui affiche tous les entiers entre 0 et lui même

```javascript
function getAllInteger(int) {
  for (let i = 0; i <= int; i++) {
    console.log(i);
  }
}

getAllInteger();

```

* Boucle qui affiche tous les entiers entre le premier et le second

```javascript
function getIntegersInInterval(int1, int2) {
  for (int1; int1 <= int2; int1++) {
    console.log(int1);
  }
}

getIntegersInInterval();

```
* Boucle qui génére un tirage de loto avec 7 nombres compris entre le min et le max

```javascript
function createRandomNumber(min, max) {
  for (let i = 1; i <= 7; i++) {
    console.log(Math.floor(min + Math.random() * (max - min)));
  }
}
createRandomNumber(1, 45);

```
* Boucle génére un tableau contenant un des nombres pairs

```javascript
const tab = [];
for (let i = 4; tab.length < 20; i = i + 2) {
  tab.push(i);
}
console.log(tab);

```
* Boucle qui génére un array avec 100 objets dont les donnée sont aléatoire

```javascript
const tab = [];

const prenom = [
  'Basile',
  'Gregoire',
  'Marie',
  'Antoine',
  'Sophie'
];

const nom = [
  'Roux',
  'Durand',
  'Richard',
  'Fournierd',
  'Michel'
];

for (let i = 0; i <= 100; i++) {
  const person = {first, last, age: rouge(1, 99)};
  const randomName = Math.floor(Math.random()*5);
  const first = prenom[randomName];
  const last = nom[randomName];
  function rouge(n, m) {
    return Math.floor(n + Math.random() * (m - n));
  }
  tab.push(person);
}
console.log(tab);

```

