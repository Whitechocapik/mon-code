# Map

* Modifier les roomName 

```javascript
const majAllRoom = hostels
    .map((hotel) => {
      hotel.rooms = hotel.rooms.map((room) => {
        room.roomName = room.roomName;
        return room;
      });
      return hotel;
    });

console.log(majAllRoom);

```

* Modifier le tableau en affichant que les noms des hotels

```javascript
const mapHostelName = hostels
    .map((hotel) => hotel.name);
console.log(mapHostelName);
```