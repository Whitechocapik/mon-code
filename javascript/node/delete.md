# Delete

* Supprimer un hotel

```javascript
    app.delete('/api/hostels/:id',
        (req, res) => {
            const id = parseInt(req.params.id);
            const deleteHostel = hostels.filter((hotel) => hotel.id !== id);
            return res.send(deleteHostel);
        });
```

* Supprimer toutes les rooms d'un hotel

```javascript
    app.delete ('/api/hostels/:id/rooms',
        (req,res) => {
            const id = parseInt((req.params.id));
            const findHostel = hostels.find((hotel) => hotel.id === id );
            let [deleteHostel] =
                findHostel.rooms = [];
            findHostel.roomNumbers = findHostel.rooms.length;
            return res.send(deleteHostel)
        });
```

* Supprimer la chambre d'un hotel

```javascript
    app.delete ('/api/hostels/:id/rooms/:id2',
        (req, res) => {
            const id = parseInt((req.params.id));
            const id2 = parseInt((req.params.id2));
            const findhotel = hostels.find((hotel) => hotel.id === id);
            const deleteRooms = findhotel.rooms.filter((room) => room.id !== id2);
            return res.send(deleteRooms);
        });
```

