# Get

* Récuperer la base de donnée

```javascript
app.get('/api/hostels', (req, res) => {
    return res.send(hostels);
});
```

*  Récuperer un hotel

```javascript
app.get('/api/hostels/:id', (req, res) => {
    const id = parseInt(req.params.id);
    const findHostel = hostels.find((hotel) => hotel.id === id);
    return res.send(findHostel);
});

```
* Récuperer toutes les rooms des hotels

```javascript
app.get('api/hostels/allRooms', (req, res) => {
    const allRooms = hostels.map((hotel) => hotel.rooms );
    return res.send(allRooms);
});

```
* Récuperer la rooms d'un hotel

```javascript
app.get('/api/hostels/:id/rooms', (req, res) => {
    const id = parseInt(req.params.id);
    const findHostel = hostels.find((hotel) => hotel.id === id);
    return res.send(findHostel.rooms);
});

```
* Récuperer la chambre de l'hotel 

```javascript
app.get('/api/hostels/:id/rooms/:id2', (req, res) => {
    const id = parseInt(req.params.id);
    const id2 = parseInt(req.params.id2);
    const findHostel = hostels.find((hotel) => hotel.id === id);
    const findRooms = findHostel.rooms.find((room) => room.id === id2);
    return res.send(findRooms);
});

```
 