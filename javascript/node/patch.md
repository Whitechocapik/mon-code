# Patch

* Changer la valeur d'un hotel

```javascript
    app.patch('/api/users/:id',
        (req, res) => {
            const newUser = req.body;
            const id = parseInt(req.params.id);
            const index = hostels
                .findIndex((hotel) => hotel.id === id);
            hostels[index] = {...hostels[index], ...newUser};
            return res.send(hostels);
        });
```
* Changer la valeur d'une rooms 

```javascript
    app.patch('/api/hostels/:id/rooms/:id',
        (req, res) => {
        const newUser = req.body;
        const  id = parseInt(req.params.id);
        const findhostel = hostels.find((hotel) => hotel.id === id);
        const index = findhostel.rooms
            .findIndex((hotel) => hotel.id === id);
        findhostel.rooms[index] = {...findhostel.rooms[index], ...newUser};
        return res.send(findhostel);
        });
```