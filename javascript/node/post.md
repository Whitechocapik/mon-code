# Post

* Ajouter un nouvel hotel

```javascript
    app.post('/api/hostel',
        (req, res) => {
            const newUser = req.body;
            const id = hostels.length + 1;
            hostels.push({...newUser, id});
            return res.send(hostels);
        });
```

* Ajoute une nouvelle chambre 

```javascript
    app.post('/api/hostels/:id/rooms',
        (req, res) => {
            const newUser = req.body;
            const id = parseInt(req.params.id);
            const findhostel = hostels.find((hotel) => hotel.id === id);
            const newId = findhostel.rooms.length + 1;
            findhostel.rooms.push({...newUser, newId});
            return res.send(hostels);
        });

```