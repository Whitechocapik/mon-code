# Put

* Remplace la ressource d'un hotel

```javascript
    app.put('/api/hostels/:id',
        (req, res) => {
            const newUser = req.body;
            const id = parseInt(req.params.id);
            const index = hostels
                .findIndex((hotel) => hotel.id === id);
            hostels[index] = {...newUser, id};
            return res.send(hostels);
        });
```

*   Remplace la ressource d'une chambre

```javascript
    app.put('/api/hostels/:id/rooms/:id2',
        (req, res) => {
        const newUser = req.body;
        const id = parseInt(req.params.id);
        const id2 = parseInt(req.params.id2);
        const findIdHostel = hostels.find((hotel) => hotel.id === id);
        const findIdRooms = findIdHostel.rooms
            .findIndex((rooms) => rooms.id === id2);
        findIdHostel.rooms[findIdRooms] = {...newUser, id2};
        return res.send(findIdHostel);
        });
```