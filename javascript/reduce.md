# Reduce

* recuperer toute les chambres dans un seul Array

```javascript
const reduceRooms = hostels
    .reduce((arr, hotel) => [...arr, ...hotel.rooms], []);

console.log(reduceRooms);

```