# Sort

* Trier les noms des hotels par le nombre de chambre 

```javascript
const sorthostel = hostels
    .sort((hotel1, hotel2) => hotel2.roomNumbers - hotel1.roomNumbers);

console.log(sorthostel);

```

* Trier les noms des hotels alphabétiquement

```javascript
const sortalphabetically = hostels
    .sort((hotel1, hotel2) => hotel2.rooms > hotel1.rooms ? +1 : -1);

console.log(sortalphabetically);

```